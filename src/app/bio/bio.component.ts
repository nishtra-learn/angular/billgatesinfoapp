import { Component, OnInit } from '@angular/core';
import { ImageModel } from "./imageModel";

@Component({
  selector: 'app-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.css']
})
export class BioComponent implements OnInit {
  image: ImageModel;

  constructor() {
    this.image = new ImageModel('../../assets/images/Bill_Gates_2018.jpg', "Bill Gates");
   }

  ngOnInit(): void {
  }

}
