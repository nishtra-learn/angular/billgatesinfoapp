import { Component, OnInit } from '@angular/core';
import { LinkModel } from "./linkModel";

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css']
})
export class LinksComponent implements OnInit {
  links: LinkModel[];

  constructor() { }

  ngOnInit(): void {
    this.links = [
      new LinkModel('https://en.wikipedia.org/wiki/Bill_Gates'),
      new LinkModel('https://www.gatesfoundation.org/'),
      new LinkModel('https://www.gatesnotes.com/'),
      new LinkModel('https://twitter.com/billgates'),
    ];
  }

}
