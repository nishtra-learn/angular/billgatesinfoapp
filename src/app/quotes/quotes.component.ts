import { Component, OnInit } from '@angular/core';
import { QuoteModel } from "./quote";

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {

  quotes: QuoteModel[];
  text:string;

  constructor() { }

  ngOnInit(): void {
    this.quotes = [
      { text: 'Patience is a key element of success.', author: 'Bill Gates' },
      { text: 'If you think your teacher is tough, wait till you get a boss.', author: 'Bill Gates' },
      { text: 'Life is not fair — get used to it', author: 'Bill Gates' },
      { text: 'Success is a lousy teacher. It seduces smart people into thinking they can’t lose.', author: 'Bill Gates' },
      { text: 'Be nice to nerds. Chances are you’ll end up working for one.', author: 'Bill Gates' },
    ];
  }

}
